//So you've decided to look into this, huh?
//Just so you know it's just a modified version of the HUD script from Modern Sonic
//Credits to MotdSpork for the original

local cv = CV_RegisterVar({name = "itemhud_size", defaultvalue = "small", flags = 0, PossibleValue = {off=0, small=1, medium=2, big=3}, func = 0}) --my first console variable

local function MRCEItemHUD(v, player)
	local client = player --don't even know if client is ever used
        if not (player.powers[pw_carry] == CR_NIGHTSMODE) --there's no NiGHTS in MRCE anyways
			local p_box 		 = v.cachePatch("BOX")
			local p_triangle 	 = v.cachePatch("TRIANGLE")
			local p_emerald	 	 = v.cachePatch("EMERLD")
			local p_fire	 	 = v.cachePatch("FIRE")
			local p_sneakers	 = v.cachePatch("SNEAK")
			local p_invinc		 = v.cachePatch("INVINC")
			local p_invins		 = v.cachePatch("INVINS")
			local p_grav		 = v.cachePatch("GRAV")
			local p_pity	 	 = v.cachePatch("PITY")
			local p_whirl		 = v.cachePatch("WHIRL")
			local p_arm			 = v.cachePatch("ARM")
			local p_pink		 = v.cachePatch("HEART")
			local p_element	 	 = v.cachePatch("ELEMENT")
			local p_attract	 	 = v.cachePatch("MAGNET")
			local p_force1		 = v.cachePatch("FORCE1")
			local p_force2		 = v.cachePatch("FORCE2")
			local p_flame		 = v.cachePatch("FLAME")
			local p_bubble		 = v.cachePatch("BUBBLE")
			local p_thunder	 	 = v.cachePatch("THUNDER")
			local p_starman	 	 = v.cachePatch("STARMAN")
			local p_starmans	 = v.cachePatch("STARMANS")
			local p_spity		 = v.cachePatch("SPITY") --small versions start here
			local p_swhirl		 = v.cachePatch("SWHIRL")
			local p_sarm		 = v.cachePatch("SARM")
			local p_spink		 = v.cachePatch("SPINK")
			local p_selement	 = v.cachePatch("SELEMENT")
			local p_sattract	 = v.cachePatch("SMAGNET")
			local p_sthunder	 = v.cachePatch("STHUNDER")
			local p_sbubble		 = v.cachePatch("SBUBBLE")
			local p_sflame		 = v.cachePatch("SFLAME")
			local p_sforce1		 = v.cachePatch("SFORCE1")
			local p_sforce2		 = v.cachePatch("SFORCE2")

			local posx --stands for position x
			local posy --if you need me to explain idk how were you even able to open this file

			local whatcolor = player.mo.color --for cool rainbow and super colors
			
			local size = FRACUNIT/2 --this is so Yume can change it
		
			posx = 307*FRACUNIT --Dajumpjumpette wanted it a "liiitle more" to the right so instead of a fixed 305 here's 307
			if G_RingSlingerGametype() --need to test this
				posy = 175*FRACUNIT
			else
				posy = 185*FRACUNIT
			end
		
			if cv.value == 0 --if itemhud_size == off (I don't think cookie really liked this whole hud thing so here's a thing to remove it
				return
			elseif	cv.value == 1 --if itemhud_size == small
				size = FRACUNIT/2
				posx = 307*FRACUNIT
				if G_RingSlingerGametype()
					posy = 175*FRACUNIT
				else
					posy = 185*FRACUNIT
				end
			elseif cv.value == 2 --if itemhud_size == medium
				size = 3*FRACUNIT/4
				posx = 303*FRACUNIT
				if G_RingSlingerGametype()
					posy = 173*FRACUNIT
				else
					posy = 183*FRACUNIT
				end
			elseif cv.value == 3 --if itemhud_size == big
				size = FRACUNIT
				posx = 300*FRACUNIT
				if G_RingSlingerGametype()
					posy = 170*FRACUNIT
				else
					posy = 180*FRACUNIT
				end
			end
		 
			//Always draw the box, the box is what we need
			v.drawScaled(posx, posy, size, p_box, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS); --snapto flags don't even fucking work so globox moment
								   --^scale for everything (0,5 of the original scale because "too big")
								   
			//Super now is overriden by shields as you can't go Super if you have a shield
			if player.powers[pw_super] and (player.mo.state != S_PLAY_SUPER_TRANS1) and (player.mo.state != S_PLAY_SUPER_TRANS2) and (player.mo.state != S_PLAY_SUPER_TRANS3) and (player.mo.state != S_PLAY_SUPER_TRANS4) and (player.mo.state != S_PLAY_SUPER_TRANS5) and (player.mo.state != S_PLAY_SUPER_TRANS6)
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", whatcolor));
				v.drawScaled(posx, posy, size, p_emerald, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			elseif All7Emeralds(emeralds)
			and player.rings >= 50
			and player.powers[pw_shield] & SH_NOSTACK == SH_NONE
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SUPERSILVER1));
				v.drawScaled(posx, posy, size, p_emerald, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//If both Invincibility and Super Sneakers
			elseif player.powers[pw_invulnerability] and player.powers[pw_sneakers]
				//Triangle shit
				if mariomode == true --Xian said it was being axed but fuck it special effects
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", whatcolor)); --RRRRRRRAAAAAAAAAAIIIIIIIIIIINNNNNNNNNBBBBBBBBBBBBOOOOOOOOOOOOOOWWWWWWWWWWWSSSSSSSSSSS
				else
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_PURPLE));
				end
				//Super Sneakers
				if player.powers[pw_sneakers] < 3*TICRATE 
					if (leveltime % 2) --flicker effect because without it "it does less of a service than vanilla"
					else	--^ courtesy of kays they explained this to me
						v.drawScaled(posx, posy, size, p_sneakers, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					end
				else
					v.drawScaled(posx, posy, size, p_sneakers, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
				end
				//Invincibility
				if player.powers[pw_invulnerability] < 3*TICRATE 
					if (leveltime % 2)
					else
						if mariomode == true
							v.drawScaled(posx, posy, size, p_starmans, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", whatcolor));
						else
							v.drawScaled(posx, posy, size, p_invins, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
						end
					end
				else
					if mariomode == true
						v.drawScaled(posx, posy, size, p_starmans, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", whatcolor));
					else
						v.drawScaled(posx, posy, size, p_invins, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					end
				end
			//Super Sneakers
			elseif player.powers[pw_sneakers]
				if player.powers[pw_sneakers] < 3*TICRATE 
					if (leveltime % 2)
					else
						v.drawScaled(posx, posy, size, p_sneakers, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					end
				else
					v.drawScaled(posx, posy, size, p_sneakers, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
				end
				//1st person thing for PV
				if player.powers[pw_shield] & SH_NOSTACK == SH_PITY
					v.drawScaled(posx, posy, size, p_spity, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_MOSS));				
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_WHIRLWIND
					v.drawScaled(posx, posy, size, p_swhirl, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_WHITE));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_ARMAGEDDON
					v.drawScaled(posx, posy, size, p_sarm, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_CRIMSON));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_PINK
					v.drawScaled(posx, posy, size, p_spink, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_ROSY));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_ELEMENTAL
					v.drawScaled(posx, posy, size, p_selement, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_APRICOT));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_ATTRACT
					v.drawScaled(posx, posy, size, p_sattract, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SUPERGOLD3));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_TUNDERCOIN or player.powers[pw_shield] & SH_NOSTACK == SH_WHIRLWIND|SH_PROTECTELECTRIC
					v.drawScaled(posx, posy, size, p_sthunder, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_BONE));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_BUBBLEWRAP
					v.drawScaled(posx, posy, size, p_sbubble, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SKY));
				elseif player.powers[pw_shield] & SH_NOSTACK == SH_FLAMEAURA
					v.drawScaled(posx, posy, size, p_sflame, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_RUST));	
				elseif player.powers[pw_shield] & SH_FORCE
					if player.powers[pw_shield] & SH_FORCEHP --Zolton said to check it inside sh_force which I guess is more optimized
						v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_NEON));	
						v.drawScaled(posx, posy, size, p_sforce1, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					else
						v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_MAGENTA));	
						v.drawScaled(posx, posy, size, p_sforce2, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					end
				else
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_RED));
				end
			//Invincibility
			elseif player.powers[pw_invulnerability]
				if mariomode == true
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", whatcolor));
				else
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SUPERSILVER1));
				end
				if player.powers[pw_invulnerability] < 3*TICRATE 
					if (leveltime % 2)
					else
						if mariomode == true
							v.drawScaled(posx, posy, size, p_starman, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
						else
							v.drawScaled(posx, posy, size, p_invinc, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
						end
					end
				else
					if mariomode == true
						v.drawScaled(posx, posy, size, p_starman, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					else
						v.drawScaled(posx, posy, size, p_invinc, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
					end
				end
			//Pity Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_PITY
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_MOSS));	
				v.drawScaled(posx, posy, size, p_pity, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Whirlwind Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_WHIRLWIND
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_WHITE));	
				v.drawScaled(posx, posy, size, p_whirl, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Armageddon Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_ARMAGEDDON
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_CRIMSON));	
				v.drawScaled(posx, posy, size, p_arm, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Amy Pity Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_PINK
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_ROSY));	
				v.drawScaled(posx, posy, size, p_pink, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Elemental Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_ELEMENTAL
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_APRICOT));	
				v.drawScaled(posx, posy, size, p_element, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Attraction Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_ATTRACT
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SUPERGOLD3));	
				v.drawScaled(posx, posy, size, p_attract, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Electric Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_TUNDERCOIN or player.powers[pw_shield] & SH_NOSTACK == SH_WHIRLWIND|SH_PROTECTELECTRIC --thundercoin alone doesn't work because lua
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_BONE));	
				v.drawScaled(posx, posy, size, p_thunder, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Bubble Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_BUBBLEWRAP
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_SKY));	
				v.drawScaled(posx, posy, size, p_bubble, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Flame Shield
			elseif player.powers[pw_shield] & SH_NOSTACK == SH_FLAMEAURA
				v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_RUST));	
				v.drawScaled(posx, posy, size, p_flame, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			//Force Shield 
			elseif player.powers[pw_shield] & SH_FORCE
				if player.powers[pw_shield] & SH_FORCEHP --Zolton said to check it inside sh_force which I guess is more optimized
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_NEON));	
					v.drawScaled(posx, posy, size, p_force1, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
				else
					v.drawScaled(posx, posy, size, p_triangle, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS, v.getColormap("-1", SKINCOLOR_MAGENTA));	
					v.drawScaled(posx, posy, size, p_force2, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
				end
			end
			//Fire Flower overlay (As it can be used with anything)
			if player.powers[pw_shield] & SH_FIREFLOWER
				v.drawScaled(posx, posy, size, p_fire, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			end
			//Gravity won't override everythinganymore
			if player.powers[pw_gravityboots]
				v.drawScaled(posx, posy, size, p_grav, V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS);
			end
		end
	end
hud.add(MRCEItemHUD)